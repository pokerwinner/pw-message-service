namespace java com.tiantian.message.thrift.message

const string version = '1.0.0'
enum MessageType{
      SHORT_MESSAGE = 0,
      EMAIL = 1,
      APPLE_PUSH = 2,
      PASSPORT = 100,
      ROOM = 201,
      GAME = 202,
      SOCKETIO = 203
}

struct MessageInfo {
  1:string toUserId,
  2:string fromUserId,
  3:string fromName,
  4:MessageType messageType,
  5:string messageEvent,
  6:string messageContent
}

service MessageService {
   string getServiceVersion(),
   string send(1:MessageInfo messageInfo),
   bool broadcastAudioInCash(1:string userId, 2:i64 size, 3:string mime, 4:string extName, 5:string vendor,
                             6:string domain, 7:string fileUrl, 8:string id, 9:string voiceTime),
   bool broadcastAudioInFriendGame(1:string userId, 2:i64 size, 3:string mime, 4:string extName, 5:string vendor,
                                                          6:string domain, 7:string fileUrl, 8:string id, 9:string voiceTime)

   bool broadcastAudioInStrangerGame(1:string userId, 2:i64 size, 3:string mime, 4:string extName, 5:string vendor,
                                     6:string domain, 7:string fileUrl, 8:string id, 9:string voiceTime)

   bool broadcastAudioInSNGGame(1:string userId, 2:i64 size, 3:string mime, 4:string extName, 5:string vendor,
                                         6:string domain, 7:string fileUrl, 8:string id, 9:string voiceTime,10:string tableId)

   bool broadcastAudioInCash2(1:string userId, 2:i64 size, 3:string mime, 4:string extName, 5:string vendor,
                             6:string domain, 7:string fileUrl, 8:string id, 9:string voiceTime, 10:string md5),
   bool broadcastAudioInFriendGame2(1:string userId, 2:i64 size, 3:string mime, 4:string extName, 5:string vendor,
                                                          6:string domain, 7:string fileUrl, 8:string id, 9:string voiceTime,
                                                          10:string md5)

   bool broadcastAudioInStrangerGame2(1:string userId, 2:i64 size, 3:string mime, 4:string extName, 5:string vendor,
                                     6:string domain, 7:string fileUrl, 8:string id, 9:string voiceTime, 10:string md5)

   bool broadcastAudioInSNGGame2(1:string userId, 2:i64 size, 3:string mime, 4:string extName, 5:string vendor,
                                         6:string domain, 7:string fileUrl, 8:string id, 9:string voiceTime,10:string tableId,
                                         11:string md5)

   bool broadcastAudioInMttGame(1:string userId, 2:i64 size, 3:string mime, 4:string extName, 5:string vendor,
                                       6:string domain, 7:string fileUrl, 8:string id, 9:string voiceTime,10:string tableId,
                                       11:string md5)

}