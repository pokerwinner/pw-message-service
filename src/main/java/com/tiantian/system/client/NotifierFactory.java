package com.tiantian.system.client;

/**
 * Created by jeffma on 15/11/11.
 */
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import com.tiantian.system.client.NotifyClient;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class NotifierFactory
        extends BasePooledObjectFactory<NotifyClient> {
    private Logger LOG = LoggerFactory.getLogger(NotifierFactory.class);
    private static final Set<Integer> RESTARTABLE_CAUSES = new HashSet<Integer>(
            Arrays.asList(TTransportException.NOT_OPEN, TTransportException.END_OF_FILE, TTransportException.TIMED_OUT,
                    TTransportException.UNKNOWN));
    @Override
    public NotifyClient create() throws Exception {
        return new NotifyClient();
    }

    @Override
    public PooledObject<NotifyClient> wrap(NotifyClient obj) {
        return new DefaultPooledObject<NotifyClient>(obj);
    }

    @Override
    public void destroyObject(PooledObject<NotifyClient> p) throws Exception {
        p.getObject().close();
    }

    @Override
    public boolean validateObject(PooledObject<NotifyClient> p) {
        NotifyClient client = p.getObject();
        String version = null;
        if(null!=client){
            try{
                version =  client.getClient().getServiceVersion();
            }catch (Exception e){
                e.printStackTrace();
                if (e instanceof TTransportException) {
                    TTransportException te = (TTransportException) e;

                    if (RESTARTABLE_CAUSES.contains(te.getType())) {

                        reconnectOrThrowException(client.getClient().getInputProtocol().getTransport());
                        try {
                            version = client.getClient().getServiceVersion();
                        } catch (TException e1) {
                            e1.printStackTrace();
                        }
                    }
                } else if (e instanceof ConnectException) {
                    reconnectOrThrowException(client.getClient().getInputProtocol().getTransport());
                    try {
                        version = client.getClient().getServiceVersion();
                    } catch (TException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        return version != null;
    }
    private void reconnectOrThrowException(TTransport transport) {
        LOG.info("NotifierFactory thrift client reconnect");
        transport.close();
        try {
            transport.open();
        }
        catch (TTransportException e) {
            e.printStackTrace();
        }
    }
}
