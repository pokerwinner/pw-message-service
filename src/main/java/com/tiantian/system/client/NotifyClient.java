package com.tiantian.system.client;

/**
 * Created by jeffma on 15/11/11.
 */

import com.tiantian.system.settings.NotifierConfig;
import com.tiantian.system.thrift.notifier.NotifierService.Client;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

public class NotifyClient{
    private TTransport transport;
    private Client client;
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }
    public NotifyClient(){
        try{
            transport = new TSocket(NotifierConfig.getInstance().getHost(),
                    NotifierConfig.getInstance().getPort());
            transport.open();
            TFastFramedTransport fastTransport = new TFastFramedTransport(transport);

            TCompactProtocol protocol = new TCompactProtocol( fastTransport);

            client = new Client(protocol);
        } catch (TException x) {
            x.printStackTrace();
        }
    }
    public void close(){
        if(null!=transport){
            transport.close();
            client = null;
        }
    }
}
