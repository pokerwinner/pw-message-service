package com.tiantian.system.utils;

/**
 * Created by jeffma on 15/11/11.
 */

import com.tiantian.system.client.NotifierFactory;
import com.tiantian.system.client.NotifyClient;
import com.tiantian.system.settings.NotifierConfig;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.util.Map;

public class NotifierUtil {
    private ObjectPool<NotifyClient> pool;
    private NotifierUtil(){
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setTestOnBorrow(NotifierConfig.getInstance().isTestOnBorrow());
        config.setTestOnCreate(NotifierConfig.getInstance().isTestOnCreate());
        pool = new GenericObjectPool<NotifyClient>(new NotifierFactory(), config);
    }
    private static class NotifierUtilHolder{
        private final static NotifierUtil instance = new NotifierUtil();
    }
    public static NotifierUtil getInstance(){
        return NotifierUtilHolder.instance;
    }
    public String getServiceVersion(){
        NotifyClient client = null;
        String version = "";
        try {
            client = pool.borrowObject();
            version=client.getClient().getServiceVersion();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return version;
    }
    public boolean notify(int type, String source, Map<String,String> detail){
        boolean result = false;
        NotifyClient client = null;
        try {
            client = pool.borrowObject();
            result=client.getClient().notify(type,source,detail);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        }finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
