package com.tiantian.message.server;

import com.tiantian.message.data.mongodb.MGDatabase;
import com.tiantian.message.handler.MessageHandler;
import com.tiantian.message.settings.MessageConfig;
import com.tiantian.message.thrift.message.MessageService;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TServerSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class MessageServer {
    private Logger LOG = LoggerFactory.getLogger(MessageServer.class);
    private TThreadPoolServer server;
    private TThreadPoolServer.Args sArgs;

    public static void main(String[] args) {
        MessageServer messageServer = new MessageServer();
        messageServer.init(args);
        messageServer.start();
    }

    public void init(String[] arguments) {
        LOG.info("init");
        try {
            MGDatabase.getInstance().init();
            MessageHandler handler = new MessageHandler();
            TProcessor tProcessor = new MessageService.Processor<MessageService.Iface>(handler);
            TServerSocket tss = new TServerSocket(MessageConfig.getInstance().getPort());
            sArgs = new TThreadPoolServer.Args(tss);
            sArgs.processor(tProcessor);
            sArgs.transportFactory(new TFastFramedTransport.Factory());
            sArgs.protocolFactory(new TCompactProtocol.Factory());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        LOG.info("start");
        server = new TThreadPoolServer(sArgs);
        LOG.info("the server listen in {}", MessageConfig.getInstance().getPort());
        server.serve();
    }

    public void stop() {
        LOG.info("stop");
        server.stop();
    }

    public void destroy() {
        LOG.info("destroy");
    }
}
