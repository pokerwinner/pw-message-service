package com.tiantian.message.client;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.thrift.TException;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by jeffma on 15/10/28.
 */
public class MessageFactory extends BasePooledObjectFactory<MessageClient> {
    private Logger LOG = LoggerFactory.getLogger(MessageFactory.class);
    private static final Set<Integer> RESTARTABLE_CAUSES = new HashSet<Integer>(
            Arrays.asList(TTransportException.NOT_OPEN, TTransportException.END_OF_FILE, TTransportException.TIMED_OUT,
                    TTransportException.UNKNOWN));
    @Override
    public MessageClient create() throws Exception {
        return new MessageClient();
    }

    @Override
    public PooledObject<MessageClient> wrap(MessageClient obj) {
        return new DefaultPooledObject<MessageClient>(obj);
    }
    @Override
    public void destroyObject(PooledObject<MessageClient> p) throws Exception {
        p.getObject().close();
    }

    @Override
    public boolean validateObject(PooledObject<MessageClient> p) {
        MessageClient client = p.getObject();
        String version = null;
        if(null!=client){
            try{
                version =  client.getClient().getServiceVersion();
            }catch (Exception e){
                e.printStackTrace();
                if (e instanceof TTransportException) {
                    TTransportException te = (TTransportException) e;

                    if (RESTARTABLE_CAUSES.contains(te.getType())) {

                        reconnectOrThrowException(client.getClient().getInputProtocol().getTransport());
                        try {
                            version = client.getClient().getServiceVersion();
                        } catch (TException e1) {
                            e1.printStackTrace();
                        }
                    }
                } else if (e instanceof ConnectException) {
                    reconnectOrThrowException(client.getClient().getInputProtocol().getTransport());
                    try {
                        version = client.getClient().getServiceVersion();
                    } catch (TException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        return StringUtils.isNotBlank(version);
    }
    private void reconnectOrThrowException(TTransport transport) {
        LOG.info("MessageFactory thrift client reconnect");
        transport.close();
        try {
            transport.open();
        }
        catch (TTransportException e) {
            e.printStackTrace();
        }
    }
}
