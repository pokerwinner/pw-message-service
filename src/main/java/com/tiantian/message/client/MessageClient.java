package com.tiantian.message.client;

import com.tiantian.message.settings.MessageConfig;
import com.tiantian.message.thrift.message.MessageService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.transport.TFastFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 * Created by jeffma on 15/10/28.
 */
public class MessageClient {
    private TTransport transport;
    private MessageService.Client client;
    public MessageService.Client getClient() {
        return client;
    }
    public void setClient(MessageService.Client client) {
        this.client = client;
    }
    public MessageClient(){
        try{
            transport = new TSocket(MessageConfig.getInstance().getHost(),
                    MessageConfig.getInstance().getPort());
            transport.open();
            TFastFramedTransport fastTransport = new TFastFramedTransport(transport);

            TCompactProtocol protocol = new TCompactProtocol( fastTransport);

            client = new MessageService.Client(protocol);
        } catch (TException x) {
            x.printStackTrace();
        }
    }
    public void close(){
        if(null!=transport){
            transport.close();
            client = null;
        }
    }
}
