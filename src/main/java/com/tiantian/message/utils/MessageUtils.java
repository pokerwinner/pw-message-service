package com.tiantian.message.utils;

import com.tiantian.message.client.MessageClient;
import com.tiantian.message.client.MessageFactory;
import com.tiantian.message.settings.MessageConfig;
import com.tiantian.message.thrift.message.MessageInfo;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

/**
 *
 */
public class MessageUtils {
    private ObjectPool<MessageClient> pool;
    private MessageUtils(){
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setTestOnBorrow(MessageConfig.getInstance().isTestOnBorrow());
        config.setTestOnCreate(MessageConfig.getInstance().isTestOnCreate());
        pool = new GenericObjectPool<MessageClient>(new MessageFactory(), config);
    }
    private static class MessageUtilsHolder{
        private final static MessageUtils instance = new MessageUtils();
    }
    public static MessageUtils getInstance(){
        return MessageUtilsHolder.instance;
    }
    public String getServiceVersion(){
        MessageClient client = null;
        String version = "";
        try {
            client = pool.borrowObject();
            version=client.getClient().getServiceVersion();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return version;
    }

    public String send(MessageInfo messageInfo) {
        MessageClient client = null;
        String result = "";
        try {
            client = pool.borrowObject();
            result = client.getClient().send(messageInfo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean broadcastAudioInCash(String userId,long size, String mime,
                                     String extName,String vendor, String domain,
                                     String fileUrl, String id, String voiceTime) {
        MessageClient client = null;
        boolean result = false;
        try {
            client = pool.borrowObject();
            result = client.getClient().broadcastAudioInCash(userId, size, mime, extName, vendor, domain, fileUrl, id, voiceTime);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean broadcastAudioInFriendGame(String userId,long size, String mime,
                                     String extName,String vendor, String domain,
                                     String fileUrl, String id, String voiceTime) {
        MessageClient client = null;
        boolean result = false;
        try {
            client = pool.borrowObject();
            result = client.getClient().broadcastAudioInFriendGame(userId, size, mime, extName, vendor, domain, fileUrl, id, voiceTime);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
    public boolean broadcastAudioInStrangerGame(String userId,long size, String mime,
                                                String extName,String vendor, String domain,
                                                String fileUrl, String id, String voiceTime) {
        MessageClient client = null;
        boolean result = false;
        try {
            client = pool.borrowObject();
            result = client.getClient().broadcastAudioInStrangerGame(userId, size, mime, extName, vendor, domain, fileUrl, id, voiceTime);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to borrow buffer from pool" + e.toString());
        } finally{
            if(null!= client){
                try {
                    pool.returnObject(client);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

}
