package com.tiantian.message.utils;

import sun.misc.BASE64Encoder;

import java.io.UnsupportedEncodingException;

/**
 *
 */
public class NotifierDataUtils {
    public static String generate(String id, String type, String data) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("id: ").append(id).append("\n");
        stringBuffer.append("type: ").append(type).append("\n");
        BASE64Encoder encoder = new BASE64Encoder();
        try {
            String b64Str = encoder.encode(data.getBytes("utf-8"));
            // b64里面有换行
            stringBuffer.append("data: ").append(b64Str.replace("\n", ""));
            stringBuffer.append("\n\n");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        return stringBuffer.toString();
    }
}
