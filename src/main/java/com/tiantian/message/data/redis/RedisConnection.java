package com.tiantian.message.data.redis;

import com.tiantian.message.settings.RedisConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by zheng on 2014/11/10.
 */
public class RedisConnection {
    private JedisPool pool;

    public static RedisConnection getInstance() {
        return RedisConnectionHolder.instance;
    }
    private static class RedisConnectionHolder{
        private final static RedisConnection instance = new RedisConnection();
    }

    public Jedis getConnection() {
        if (pool == null) {
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            pool = new JedisPool(jedisPoolConfig, RedisConfig.getInstance().getHost(),
                    RedisConfig.getInstance().getPort());
        }
        return pool.getResource();
    }

    public Jedis getConnection(String host, int port) {
        if (pool == null) {
            pool = new JedisPool(new JedisPoolConfig(), host, port);
        }
        return pool.getResource();
    }

    private RedisConnection() {

    }
}
