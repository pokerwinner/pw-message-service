package com.tiantian.message.settings;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.bpsm.edn.parser.Parseable;
import us.bpsm.edn.parser.Parser;
import us.bpsm.edn.parser.Parsers;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;
import static us.bpsm.edn.Keyword.newKeyword;
import static us.bpsm.edn.parser.Parsers.defaultConfiguration;

/**
 *
 */
public class MessageConfig {
    private Logger LOG= LoggerFactory.getLogger(MessageConfig.class);
    private String configFile = "config/message.edn";
    private Map<?, ?> appConf;
    private int port;
    private String host;
    private String env;
    private boolean testOnBorrow;
    private boolean testOnCreate;
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
    }

    public boolean isTestOnBorrow() {
        return testOnBorrow;
    }

    public void setTestOnBorrow(boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public boolean isTestOnCreate() {
        return testOnCreate;
    }

    public void setTestOnCreate(boolean testOnCreate) {
        this.testOnCreate = testOnCreate;
    }

    private MessageConfig(){
        env = System.getenv("NOMAD_ENV");
        if(null == env || "" == env){
            env = "development";
        }
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream(configFile);
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        LOG.info("==> config {}",configString);
        LOG.info("env is {}",env);
        Parseable pbr = Parsers.newParseable(configString);
        Parser p = Parsers.newParser(defaultConfiguration());
        appConf = (Map<?, ?>) p.nextValue(pbr);
        Map<?, ?> envs = (Map<?, ?>)appConf.get(newKeyword("nomad","environments"));
        Map<?, ?> appEnv = (Map<?, ?>)envs.get(env);
        this.port = Integer.parseInt(String.valueOf(appEnv.get(newKeyword("port"))));
        this.host = String.valueOf(appEnv.get(newKeyword("host")));
        this.testOnBorrow = Boolean.valueOf(String.valueOf(appEnv.get(newKeyword("testOnBorrow"))));
        this.testOnCreate = Boolean.valueOf(String.valueOf(appEnv.get(newKeyword("testOnCreate"))));
        LOG.info("port is {}", this.port);
        LOG.info("host is {}", this.host);
    }
    private static class MessageConfigHolder{
        private final static MessageConfig instance = new MessageConfig();
    }
    public static MessageConfig getInstance(){
        return MessageConfigHolder.instance;
    }
}
