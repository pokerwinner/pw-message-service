package com.tiantian.message.settings;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.bpsm.edn.parser.Parseable;
import us.bpsm.edn.parser.Parser;
import us.bpsm.edn.parser.Parsers;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;

import static us.bpsm.edn.Keyword.newKeyword;
import static us.bpsm.edn.parser.Parsers.defaultConfiguration;

/**
 * Created by lichenghu on 15-11-6.
 */
public class RedisConfig {
    private Logger LOG= LoggerFactory.getLogger(RedisConfig.class);
    private String configFile = "config/redis.edn";
    private Map<?, ?> appConf;
    private String host;
    private int port;
    private String env;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    private RedisConfig(){
        env = System.getenv("NOMAD_ENV");
        if(null == env || "" == env){
            env = "development";
        }
        InputStream inputConfig = this.getClass().getClassLoader().getResourceAsStream(configFile);
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputConfig, writer, "utf8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        String configString = writer.toString();
        LOG.info("==> redis config {}",configString);
        LOG.info("env is {}",env);
        Parseable pbr = Parsers.newParseable(configString);
        Parser p = Parsers.newParser(defaultConfiguration());
        appConf = (Map<?, ?>) p.nextValue(pbr);
        Map<?, ?> envs = (Map<?, ?>)appConf.get(newKeyword("nomad","environments"));
        Map<?, ?> appEnv = (Map<?, ?>)envs.get(env);
        this.port = Integer.parseInt(String.valueOf(appEnv.get(newKeyword("port"))));
        this.host = String.valueOf(appEnv.get(newKeyword("host")));

        LOG.info("redis port is {}", this.port);
        LOG.info("redis host is {}", this.host);

    }
    private static class RedisConfigHolder {
        private final static RedisConfig instance = new RedisConfig();
    }
    public static RedisConfig getInstance(){
        return RedisConfigHolder.instance;
    }
}
