package com.tiantian.message.handler;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mongodb.client.MongoCollection;
import com.tiantian.message.data.mongodb.MGDatabase;
import com.tiantian.message.data.redis.RedisUtil;
import com.tiantian.message.thrift.message.MessageInfo;
import com.tiantian.message.thrift.message.MessageService;
import com.tiantian.message.thrift.message.MessageType;
import com.tiantian.message.thrift.message.messageConstants;
import com.tiantian.message.utils.NotifierDataUtils;
import com.tiantian.system.utils.NotifierUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 *
 */
public class MessageHandler implements MessageService.Iface {

    // map key
    private static final String DATA_STR = "dataStr";
    private static final String EVENT = "event";
    private static final String SERVER_ID = "serverId";
    private static final  String TO = "to";
    private static String ROUTER_KEY = "router_key:";

    private static String AUDIOS_TABLE = "audios_file";

    private static String USER_ROOM_TABLE_KEY = "user_room_table_key:";
    // 桌子在线玩家
    private static String TABLE_USER_ONLINE_KEY = "table_user_online_key:";


    private static String USER_GROUP_TABLE_KEY = "user_group_table_key:";
    private static String GROUP_TABLE_USER_ONLINE_KEY = "group_table_user_online_key:";

    // 桌子上玩家
    private String USER_STRANGER_TABLE_KEY = "user_stranger_table_key:";
    // 桌子在线玩家
    private String STRANGER_TABLE_USER_ONLINE_KEY = "stranger_table_user_online_key:";

    // 桌子上玩家
    private String USER_SNG_TABLE_KEY = "user_sng_table_key:";
    // 桌子在线玩家
    private String SNG_TABLE_USER_ONLINE_KEY = "sng_table_user_online_key:";

    private String MTT_TABLE_USER_ONLINE_KEY = "club_mtt_table_user_online_key:";

    private String MTT_TABLE_STATUS_KEY = "club_mtt_table_status:";

    @Override
    public String getServiceVersion() throws TException {
        return messageConstants.version;
    }

    @Override
    public String send(MessageInfo messageInfo) throws TException {
        if (messageInfo == null || messageInfo.getToUserId() == null) {
            return "-1";
        }
        MessageType type = messageInfo.getMessageType();
        String data = NotifierDataUtils.generate(UUID.randomUUID().toString().replace("-", ""),
                type.toString(), messageInfo.getMessageContent());
        // 传输数据
        Map<String, String> dataMap = new HashMap<>();
        dataMap.put(DATA_STR, data);
        dataMap.put(EVENT, messageInfo.getMessageEvent());

        String serverId = RedisUtil.get(ROUTER_KEY + messageInfo.getToUserId());
        if (StringUtils.isBlank(serverId)) {
            return "0";
        }
        // 分发到指定的服务器上
        dataMap.put(SERVER_ID, serverId);
        dataMap.put(TO, messageInfo.getToUserId());
        try {
            // 分发到game的队列中
            NotifierUtil.getInstance().notify(type.getValue(), "MessageService", dataMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }


    // 现金桌里面的广播音频
    public boolean broadcastAudioInCash(String userId,long size, String mime,
                                     String extName,String vendor, String domain,
                                     String fileUrl, String id, String voiceTime) throws TException {
        //
        String tableId = RedisUtil.getFromMap(USER_ROOM_TABLE_KEY + userId, "tableId");
        if(StringUtils.isNotBlank(tableId)) {
            Map<String, String> tableAllUserMap = RedisUtil.getMap(TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                Set<String> onlineTableUserIds = tableAllUserMap.keySet();
                if (onlineTableUserIds != null && onlineTableUserIds.size() > 0) {
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setMessageEvent("audio_broadcast");
                    messageInfo.setMessageType(MessageType.GAME);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("audio_url", fileUrl);
                    jsonObject.put("id", id);
                    jsonObject.put("size", size);
                    jsonObject.put("user_id", userId);
                    jsonObject.put("voice_time", Float.parseFloat(voiceTime));
                    if (StringUtils.isNotBlank(tableId)) {
                        jsonObject.put("unique_id", tableId);
                    }
                    messageInfo.setMessageContent(jsonObject.toJSONString());
                    for (String $userId : onlineTableUserIds) {
                         messageInfo.setToUserId($userId);
                         send(messageInfo);
                    }
                }
            }
            // 保存mongo
            MongoCollection<Document> logsCollection = MGDatabase.getInstance().getStoreDB().getCollection(AUDIOS_TABLE);
            Document document = new Document();
            document.put("_id", new ObjectId(id));
            document.put("userId", userId);
            document.put("type", "cash");
            document.put("size", size);
            document.put("mime", mime);
            document.put("extName", extName);
            document.put("vendor", vendor);
            document.put("domain", domain);
            document.put("fileUrl", fileUrl);
            long createDate = System.currentTimeMillis();
            document.put("createDate", createDate);
            logsCollection.insertOne(document);
            return true;
        }
        return false;
    }

    // 朋友场里面的广播音频
    public boolean broadcastAudioInFriendGame(String userId,long size, String mime,
                                           String extName,String vendor, String domain,
                                           String fileUrl, String id, String voiceTime) throws TException {
        String tableId = RedisUtil.getFromMap(USER_GROUP_TABLE_KEY + userId, "tableId");
        if(StringUtils.isNotBlank(tableId)) {
            Map<String, String> tableAllUserMap = RedisUtil.getMap(GROUP_TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                Set<String> onlineTableUserIds = tableAllUserMap.keySet();
                if (onlineTableUserIds != null && onlineTableUserIds.size() > 0) {
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setMessageEvent("friend_audio_broadcast");
                    messageInfo.setMessageType(MessageType.GAME);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("audio_url", fileUrl);
                    jsonObject.put("id", id);
                    jsonObject.put("user_id", userId);
                    jsonObject.put("size", size);
                    jsonObject.put("voice_time", Float.parseFloat(voiceTime));
                    if (StringUtils.isNotBlank(tableId)) {
                        jsonObject.put("unique_id", tableId);
                    }
                    messageInfo.setMessageContent(jsonObject.toJSONString());
                    for (String $userId : onlineTableUserIds) {
                         messageInfo.setToUserId($userId);
                         send(messageInfo);
                    }
                }
            }
            // 保存mongo
            MongoCollection<Document> logsCollection = MGDatabase.getInstance().getStoreDB().getCollection(AUDIOS_TABLE);
            Document document = new Document();
            document.put("_id", new ObjectId(id));
            document.put("userId", userId);
            document.put("type", "friend");
            document.put("size", size);
            document.put("mime", mime);
            document.put("extName", extName);
            document.put("vendor", vendor);
            document.put("domain", domain);
            document.put("fileUrl", fileUrl);
            long createDate = System.currentTimeMillis();
            document.put("createDate", createDate);
            logsCollection.insertOne(document);
            return true;
        }
        return false;
    }

    // 陌生人场里面的广播音频
    public boolean broadcastAudioInStrangerGame(String userId,long size, String mime,
                                              String extName,String vendor, String domain,
                                              String fileUrl, String id, String voiceTime) throws TException {
        String tableId = RedisUtil.getFromMap(USER_STRANGER_TABLE_KEY + userId, "tableId");
        if(StringUtils.isNotBlank(tableId)) {
            Map<String, String> tableAllUserMap = RedisUtil.getMap(STRANGER_TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                Set<String> onlineTableUserIds = tableAllUserMap.keySet();
                if (onlineTableUserIds != null && onlineTableUserIds.size() > 0) {
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setMessageEvent("stranger_audio_broadcast");
                    messageInfo.setMessageType(MessageType.GAME);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("audio_url", fileUrl);
                    jsonObject.put("id", id);
                    jsonObject.put("user_id", userId);
                    jsonObject.put("size", size);
                    jsonObject.put("voice_time", Float.parseFloat(voiceTime));
                    if (StringUtils.isNotBlank(tableId)) {
                        jsonObject.put("unique_id", tableId);
                    }
                    messageInfo.setMessageContent(jsonObject.toJSONString());
                    for (String $userId : onlineTableUserIds) {
                        messageInfo.setToUserId($userId);
                        send(messageInfo);
                    }
                }
            }
            // 保存mongo
            MongoCollection<Document> logsCollection = MGDatabase.getInstance().getStoreDB().getCollection(AUDIOS_TABLE);
            Document document = new Document();
            document.put("_id", new ObjectId(id));
            document.put("userId", userId);
            document.put("type", "stranger");
            document.put("size", size);
            document.put("mime", mime);
            document.put("extName", extName);
            document.put("vendor", vendor);
            document.put("domain", domain);
            document.put("fileUrl", fileUrl);
            long createDate = System.currentTimeMillis();
            document.put("createDate", createDate);
            logsCollection.insertOne(document);
            return true;
        }
        return false;
    }

    // SNG场里面的广播音频
    public boolean broadcastAudioInSNGGame(String userId,long size, String mime,
                                                String extName,String vendor, String domain,
                                                String fileUrl, String id, String voiceTime, String tableId) throws TException {
        if(StringUtils.isNotBlank(tableId)) {
            Map<String, String> tableAllUserMap = RedisUtil.getMap(SNG_TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                Set<String> onlineTableUserIds = tableAllUserMap.keySet();
                if (onlineTableUserIds.size() > 0) {
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setMessageEvent("sng_audio_broadcast");
                    messageInfo.setMessageType(MessageType.GAME);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("audio_url", fileUrl);
                    jsonObject.put("id", id);
                    jsonObject.put("user_id", userId);
                    jsonObject.put("size", size);
                    jsonObject.put("voice_time", Float.parseFloat(voiceTime));
                    if (StringUtils.isNotBlank(tableId)) {
                        jsonObject.put("unique_id", tableId);
                    }
                    messageInfo.setMessageContent(jsonObject.toJSONString());
                    for (String $userId : onlineTableUserIds) {
                        messageInfo.setToUserId($userId);
                        send(messageInfo);
                    }
                }
            }
            // 保存mongo
            MongoCollection<Document> logsCollection = MGDatabase.getInstance().getStoreDB().getCollection(AUDIOS_TABLE);
            Document document = new Document();
            document.put("_id", new ObjectId(id));
            document.put("userId", userId);
            document.put("type", "sng");
            document.put("size", size);
            document.put("mime", mime);
            document.put("extName", extName);
            document.put("vendor", vendor);
            document.put("domain", domain);
            document.put("fileUrl", fileUrl);
            long createDate = System.currentTimeMillis();
            document.put("createDate", createDate);
            logsCollection.insertOne(document);
            return true;
        }
        return false;
    }

    @Override
    public boolean broadcastAudioInCash2(String userId, long size, String mime, String extName, String vendor, String domain, String fileUrl, String id, String voiceTime, String md5) throws TException {
        //
        String tableId = RedisUtil.getFromMap(USER_ROOM_TABLE_KEY + userId, "tableId");
        if(StringUtils.isNotBlank(tableId)) {
            Map<String, String> tableAllUserMap = RedisUtil.getMap(TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                Set<String> onlineTableUserIds = tableAllUserMap.keySet();
                if (onlineTableUserIds != null && onlineTableUserIds.size() > 0) {
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setMessageEvent("audio_broadcast");
                    messageInfo.setMessageType(MessageType.GAME);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("audio_url", fileUrl);
                    jsonObject.put("id", id);
                    jsonObject.put("size", size);
                    jsonObject.put("user_id", userId);
                    jsonObject.put("voice_time", Float.parseFloat(voiceTime));
                    jsonObject.put("md5", md5 == null ? "" : md5);
                    if (StringUtils.isNotBlank(tableId)) {
                        jsonObject.put("unique_id", tableId);
                    }
                    messageInfo.setMessageContent(jsonObject.toJSONString());
                    for (String $userId : onlineTableUserIds) {
                        messageInfo.setToUserId($userId);
                        send(messageInfo);
                    }
                }
            }
            // 保存mongo
            MongoCollection<Document> logsCollection = MGDatabase.getInstance().getStoreDB().getCollection(AUDIOS_TABLE);
            Document document = new Document();
            document.put("_id", new ObjectId(id));
            document.put("userId", userId);
            document.put("type", "cash");
            document.put("size", size);
            document.put("mime", mime);
            document.put("extName", extName);
            document.put("vendor", vendor);
            document.put("domain", domain);
            document.put("fileUrl", fileUrl);
            document.put("md5", md5 == null ? "" : md5);
            long createDate = System.currentTimeMillis();
            document.put("createDate", createDate);
            logsCollection.insertOne(document);
            return true;
        }
        return false;
    }

    @Override
    public boolean broadcastAudioInFriendGame2(String userId, long size, String mime, String extName, String vendor, String domain, String fileUrl, String id, String voiceTime, String md5) throws TException {
        String tableId = RedisUtil.getFromMap(USER_GROUP_TABLE_KEY + userId, "tableId");
        if(StringUtils.isNotBlank(tableId)) {
            Map<String, String> tableAllUserMap = RedisUtil.getMap(GROUP_TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                Set<String> onlineTableUserIds = tableAllUserMap.keySet();
                if (onlineTableUserIds != null && onlineTableUserIds.size() > 0) {
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setMessageEvent("friend_audio_broadcast");
                    messageInfo.setMessageType(MessageType.GAME);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("audio_url", fileUrl);
                    jsonObject.put("id", id);
                    jsonObject.put("user_id", userId);
                    jsonObject.put("size", size);
                    jsonObject.put("md5", md5 == null ? "" : md5);
                    jsonObject.put("voice_time", Float.parseFloat(voiceTime));
                    if (StringUtils.isNotBlank(tableId)) {
                        jsonObject.put("unique_id", tableId);
                    }
                    messageInfo.setMessageContent(jsonObject.toJSONString());
                    for (String $userId : onlineTableUserIds) {
                        messageInfo.setToUserId($userId);
                        send(messageInfo);
                    }
                }
            }
            // 保存mongo
            MongoCollection<Document> logsCollection = MGDatabase.getInstance().getStoreDB().getCollection(AUDIOS_TABLE);
            Document document = new Document();
            document.put("_id", new ObjectId(id));
            document.put("userId", userId);
            document.put("type", "friend");
            document.put("size", size);
            document.put("mime", mime);
            document.put("extName", extName);
            document.put("vendor", vendor);
            document.put("domain", domain);
            document.put("fileUrl", fileUrl);
            document.put("md5", md5 == null ? "" : md5);
            long createDate = System.currentTimeMillis();
            document.put("createDate", createDate);
            logsCollection.insertOne(document);
            return true;
        }
        return false;
    }

    @Override
    public boolean broadcastAudioInStrangerGame2(String userId, long size, String mime, String extName, String vendor, String domain, String fileUrl, String id, String voiceTime, String md5) throws TException {
        String tableId = RedisUtil.getFromMap(USER_STRANGER_TABLE_KEY + userId, "tableId");
        if(StringUtils.isNotBlank(tableId)) {
            Map<String, String> tableAllUserMap = RedisUtil.getMap(STRANGER_TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                Set<String> onlineTableUserIds = tableAllUserMap.keySet();
                if (onlineTableUserIds != null && onlineTableUserIds.size() > 0) {
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setMessageEvent("stranger_audio_broadcast");
                    messageInfo.setMessageType(MessageType.GAME);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("audio_url", fileUrl);
                    jsonObject.put("id", id);
                    jsonObject.put("user_id", userId);
                    jsonObject.put("size", size);
                    jsonObject.put("md5", md5 == null ? "" : md5);
                    jsonObject.put("voice_time", Float.parseFloat(voiceTime));
                    if (StringUtils.isNotBlank(tableId)) {
                        jsonObject.put("unique_id", tableId);
                    }
                    messageInfo.setMessageContent(jsonObject.toJSONString());
                    for (String $userId : onlineTableUserIds) {
                        messageInfo.setToUserId($userId);
                        send(messageInfo);
                    }
                }
            }
            // 保存mongo
            MongoCollection<Document> logsCollection = MGDatabase.getInstance().getStoreDB().getCollection(AUDIOS_TABLE);
            Document document = new Document();
            document.put("_id", new ObjectId(id));
            document.put("userId", userId);
            document.put("type", "stranger");
            document.put("size", size);
            document.put("mime", mime);
            document.put("extName", extName);
            document.put("vendor", vendor);
            document.put("domain", domain);
            document.put("fileUrl", fileUrl);
            document.put("md5", md5 == null ? "" : md5);
            long createDate = System.currentTimeMillis();
            document.put("createDate", createDate);
            logsCollection.insertOne(document);
            return true;
        }
        return false;
    }

    @Override
    public boolean broadcastAudioInSNGGame2(String userId, long size, String mime, String extName, String vendor, String domain, String fileUrl, String id, String voiceTime, String tableId, String md5) throws TException {
        if(StringUtils.isNotBlank(tableId)) {
            Map<String, String> tableAllUserMap = RedisUtil.getMap(SNG_TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                Set<String> onlineTableUserIds = tableAllUserMap.keySet();
                if (onlineTableUserIds.size() > 0) {
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setMessageEvent("sng_audio_broadcast");
                    messageInfo.setMessageType(MessageType.GAME);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("audio_url", fileUrl);
                    jsonObject.put("id", id);
                    jsonObject.put("user_id", userId);
                    jsonObject.put("size", size);
                    jsonObject.put("md5", md5 == null ? "" : md5);
                    jsonObject.put("voice_time", Float.parseFloat(voiceTime));
                    if (StringUtils.isNotBlank(tableId)) {
                        jsonObject.put("unique_id", tableId);
                    }
                    messageInfo.setMessageContent(jsonObject.toJSONString());
                    for (String $userId : onlineTableUserIds) {
                        messageInfo.setToUserId($userId);
                        send(messageInfo);
                    }
                }
            }
            // 保存mongo
            MongoCollection<Document> logsCollection = MGDatabase.getInstance().getStoreDB().getCollection(AUDIOS_TABLE);
            Document document = new Document();
            document.put("_id", new ObjectId(id));
            document.put("userId", userId);
            document.put("type", "sng");
            document.put("size", size);
            document.put("mime", mime);
            document.put("extName", extName);
            document.put("vendor", vendor);
            document.put("domain", domain);
            document.put("fileUrl", fileUrl);
            document.put("md5", md5 == null ? "" : md5);
            long createDate = System.currentTimeMillis();
            document.put("createDate", createDate);
            logsCollection.insertOne(document);
            return true;
        }
        return false;
    }


    public boolean broadcastAudioInMttGame(String userId, long size, String mime, String extName, String vendor,
                                           String domain, String fileUrl, String id, String voiceTime,
                                           String tableId, String md5) throws TException {
        if(StringUtils.isNotBlank(tableId)) {
            Map<String, String> tableAllUserMap = RedisUtil.getMap(MTT_TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                Set<String> onlineTableUserIds = tableAllUserMap.keySet();
                String gameId = RedisUtil.getFromMap(MTT_TABLE_STATUS_KEY + tableId, "roomId");
                if (onlineTableUserIds.size() > 0) {
                    MessageInfo messageInfo = new MessageInfo();
                    messageInfo.setMessageEvent("mtt_audio_broadcast");
                    messageInfo.setMessageType(MessageType.GAME);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("audio_url", fileUrl);
                    jsonObject.put("id", id);
                    jsonObject.put("user_id", userId);
                    jsonObject.put("size", size);
                    jsonObject.put("md5", md5 == null ? "" : md5);
                    jsonObject.put("voice_time", Float.parseFloat(voiceTime));
                    if (StringUtils.isNotBlank(gameId)) {
                        jsonObject.put("unique_id", gameId);
                    }
                    messageInfo.setMessageContent(jsonObject.toJSONString());
                    for (String $userId : onlineTableUserIds) {
                        messageInfo.setToUserId($userId);
                        send(messageInfo);
                    }
                }
            }
            // 保存mongo
            MongoCollection<Document> logsCollection = MGDatabase.getInstance().getStoreDB().getCollection(AUDIOS_TABLE);
            Document document = new Document();
            document.put("_id", new ObjectId(id));
            document.put("userId", userId);
            document.put("type", "sng");
            document.put("size", size);
            document.put("mime", mime);
            document.put("extName", extName);
            document.put("vendor", vendor);
            document.put("domain", domain);
            document.put("fileUrl", fileUrl);
            document.put("md5", md5 == null ? "" : md5);
            long createDate = System.currentTimeMillis();
            document.put("createDate", createDate);
            logsCollection.insertOne(document);
            return true;
        }
        return false;
    }
}
